const int trigger=7;
const int echo=6;
const int ledGREEN = 8;
const int ledYELLOW = 9;
const int ledRED = 10;
const int ledSOUND = 11;

const int STATUS_GREEN = 0;
const int STATUS_YELLOW = 1;
const int STATUS_RED = 2;

const long measureIntervall = 1000;
const long signalIntervallGREEN = 1000;
const long signalIntervallYELLOW = 500; 
const long signalIntervallRED = 100;

long lastMeasureMs = 0;
long lastSignalMs = 0;
int currentState = STATUS_GREEN;
int currentSignalInvervall = signalIntervallGREEN;
bool isOn = true;
 
void setup()
{
  Serial.begin (9600);

  pinMode(ledSOUND,OUTPUT);
  pinMode(ledGREEN,OUTPUT);
  pinMode(ledYELLOW,OUTPUT);
  pinMode(ledRED,OUTPUT);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
}
 
void loop()
{
  long distance;
  unsigned long actualMs = millis();
  
  // check timer for distance measuring 
  if ( actualMs - lastMeasureMs >= measureIntervall)
  {
    // get distance
    distance = getDistance();
    lastMeasureMs = actualMs;

    // print actual distance
    printDistance(distance);
 
    // now check the ranges
    if (distance >= 10)
    {
      currentState = STATUS_GREEN;
      currentSignalInvervall = signalIntervallGREEN;
    } else if (distance >= 5)
    {
      currentState = STATUS_YELLOW;
      currentSignalInvervall = signalIntervallYELLOW;
    } else
    {
      currentState = STATUS_RED;
      currentSignalInvervall = signalIntervallRED;
    }
  }

  // check if signal timer is run up
  if (actualMs - lastSignalMs >= currentSignalInvervall)
  {
    if (isOn == true) 
    {
      digitalWrite(ledSOUND,HIGH);
      
      if (currentState == STATUS_GREEN)
      {
        statusGREEN();
      }
      if (currentState == STATUS_YELLOW)
      {
        statusYELLOW();
      }
      if (currentState == STATUS_RED)
      {
        statusRED();
      }
       isOn = false;
       Serial.println("Eingeschaltet");
    }
    else
    {
      digitalWrite(ledGREEN,LOW);
      digitalWrite(ledYELLOW,LOW);
      digitalWrite(ledRED,LOW);
      digitalWrite(ledSOUND,LOW);
      
      isOn = true;
      Serial.println("Ausgeschaltet");
    }
    lastSignalMs = actualMs;
  }
  
}

int getDistance()
{
  long dauer=0;
  long entfernung=0;
  
  digitalWrite(trigger, LOW);
  delay(5);
  digitalWrite(trigger, HIGH);
  delay(10);
  digitalWrite(trigger, LOW);
  
  dauer = pulseIn(echo, HIGH);
   
  entfernung = (dauer/2) / 29.1;

  return entfernung;
}

// -----------------------------------------------------
void printDistance(int distance)
{
    if (distance >= 500 || distance <= 0)
  {
    Serial.println("Kein Messwert");
  }
  else 
  {
    Serial.print(distance);
    Serial.println(" cm");
  }
}

// -----------------------------------------------------
void statusGREEN()
{
  digitalWrite(ledGREEN,HIGH);
  digitalWrite(ledYELLOW,LOW);
  digitalWrite(ledRED,LOW);
}

// -----------------------------------------------------
void statusYELLOW()
{
  digitalWrite(ledGREEN,HIGH);
  digitalWrite(ledYELLOW,HIGH);
  digitalWrite(ledRED,LOW);
}

// -----------------------------------------------------
void statusRED()
{
  digitalWrite(ledGREEN,HIGH);
  digitalWrite(ledYELLOW,HIGH);
  digitalWrite(ledRED,HIGH);
}
  
