#include <DS3231.h>
#include <Wire.h>    // I2C-Bibliothek einbinden
#include <SPI.h>
#include <SD.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>

// defines for hardware
#define DHTPIN 12 
#define DHTTYPE DHT22 
#define LED_SDCARD_WRITE 8
#define LED_SHOW_ERROR 13
#define SD_CARD_CHIP_SELECT 4
#define BACKLIGHT_PIN 7
#define VBATPIN A9

// defines for main program
#define READ_INTERVALL_MINS 5ul

// constants 

// global objects..
DS3231  RTC(SDA, SCL);
DHT dht(DHTPIN, DHTTYPE);               // for sensor unit
//                    addr, en,rw,rs,d4,d5,d6,d7,bl, blpol
LiquidCrystal_I2C lcd(0x27,  2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);   // the LCD display


unsigned long lastSDWrite;              // timer counter
unsigned long timerIntervall;
 
void setup(void) {
  
  // start serial output
  Serial.begin(9600);

  // Greetings on the monitor
  Serial.println(F("System starts..."));

  // initialize LCD display
  initializeLCD();
  
  // initialize RTC
  RTC.begin();  
  
  // initialize sensor unit
  dht.begin();

  // initialize SD card
  initializeSD();
  
  // set pins for information
  pinMode(LED_SDCARD_WRITE,OUTPUT);
  pinMode(LED_SHOW_ERROR,OUTPUT);
  
  //lcd.clear();
  lastSDWrite = millis();
  timerIntervall = READ_INTERVALL_MINS * 60 * 1000;

  Serial.println(F("Nu geit datt los..."));
  Serial.println(F("\n---------------------------------------------\n"));

  lcd.clear();
}

// ---------------------------------------------------------------------------------------
void initializeLCD() {

  Serial.println(F("Initialize LCD"));

  lcd.begin (16,2);
  
  lcd.print(F("System"));
  lcd.setCursor(0,1); 
  lcd.print(F("initializes"));

  Serial.println("LCD initialized");
}

// ---------------------------------------------------------------------------------------
void initializeSD() {
  
  Serial.println(F("\nInitialize SD card..."));

  // see if the card is present and can be initialized:
  if (!SD.begin(SD_CARD_CHIP_SELECT)) {
    Serial.println(F("SD not present or could not be opened"));
    // set warning light
    digitalWrite(LED_SHOW_ERROR,HIGH);
    return;
  }
  Serial.println(F("SD card intialized"));
}
// ---------------------------------------------------------------------------------------
void loop () {

  String currentTime;
  float currHumidity;
  float currTemperature;

  // get data from sensor
  currHumidity = dht.readHumidity();     //Luftfeuchte auslesen
  currTemperature = dht.readTemperature();  //Temperatur auslesen

  // check for correct number. if NaN (not a number) show error
  if (isnan(currTemperature) || isnan(currHumidity)) {
    Serial.println(F("Unable to read from DHT22"));
  } 
  else {

    // print on LCD display
    printCurrentData(currTemperature, currHumidity);
    
    // check timer
    if (millis() - lastSDWrite > timerIntervall) { 

      // save current time for timer
      lastSDWrite = millis();

      // create timestamp
      currentTime = createTimeStamp();
  
      // write to SD card timestamp and data
      writeDataToSD(currentTime, currTemperature, currHumidity,measureBattery());   
    }
  }

  delay(1000);
}

// ---------------------------------------------------------------------------------------
void printCurrentData(float temperature, float humidity) {
  
  lcd.setCursor(0,0); 
  lcd.print("C: ");
  lcd.setCursor(4,0); 
  lcd.print(String(temperature,2));
  lcd.setCursor(0,1);
  lcd.print("%: ");
  lcd.setCursor(4,1);
  lcd.print(String(humidity,2));
}

// ---------------------------------------------------------------------------------------
String writeDataToSD( String timeStamp, float temperature, float humidity, float volt) {

  // start writinge LED and open file
  digitalWrite(LED_SDCARD_WRITE,HIGH);
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {

    // create log entry
    timeStamp.concat(";");
    timeStamp.concat(String(temperature,2));
    timeStamp.concat(";");
    timeStamp.concat(String(humidity,2));
    timeStamp.concat(";");
    timeStamp.concat(String(volt,2));

    // write data and close file again
    dataFile.println(timeStamp);
    dataFile.close(); 

    // log to serial monitor as well
    Serial.println(timeStamp);

    // clear write LED
    digitalWrite(LED_SDCARD_WRITE,LOW);
  }
  else {
     // if the file isn't open, pop up an error:
    Serial.println(F("error opening datalog.txt"));
  }

  return timeStamp;
}

// ---------------------------------------------------------------------------------------
String createTimeStamp() {
  
  String timeStamp;

  timeStamp = String(RTC.getDateStr(FORMAT_LONG, FORMAT_BIGENDIAN,'-'));
  timeStamp = String(timeStamp + " ");
  timeStamp = String(timeStamp + RTC.getTimeStr(FORMAT_LONG));

  return timeStamp;
}


// ---------------------------------------------------------------------------------------
float measureBattery() {
   
    float measuredvbat = analogRead(VBATPIN);
    
    measuredvbat *= 2;    // we divided by 2, so multiply back
    measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
    measuredvbat /= 1024; // convert to voltage

    return measuredvbat;
    
}

