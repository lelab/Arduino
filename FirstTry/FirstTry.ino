int pinLed = 6;
int pinTaster = 7;
int tasterStatus = 0;

void setup()
{
  pinMode(pinLed, OUTPUT);
  pinMode(pinTaster, INPUT);
}

void loop()
{
  tasterStatus = digitalRead(pinTaster);

  if (tasterStatus == HIGH)
  {
    digitalWrite(pinLed, HIGH);
    delay(1000);
    digitalWrite(pinLed, LOW);
  }
  else
  {
    digitalWrite(pinLed, LOW);
  }
}
