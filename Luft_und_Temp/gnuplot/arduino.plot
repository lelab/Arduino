set term png size 3500,2000
set output 'Luft_und_Temperaturverlauf.png'
set datafile separator ";"
set timefmt '%Y-%m-%d %H:%M:%S'
set format x "%Hh\n%a\n%d.%m."
set xdata time
set title "Messung des Raumklimas"
set grid x y2
set ytics nomirror
set y2tics
set tics out
set xtics format "%Hh\n%a\n%d.%m."
set xtics "2016-01-11 00:00:00", 86400, "2016-03-25 00:00:00"
set autoscale y
set autoscale y2
set ylabel "Temperatur"
set y2label "rel. Luftfeuchtigkeit"
plot "datalog.txt" using 1:2 title '�C' with lines, "datalog.txt" using 1:3 title '%RH' with lines