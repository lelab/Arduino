#include <Wire.h>    // I2C-Bibliothek einbinden
#include <RTClib.h>  // RTC-Bibliothek einbinden
#include <SPI.h>
#include <SD.h>
#include <LiquidCrystal.h>
#include <DHT.h>

// defines
#define DHTPIN 5  
#define DHTTYPE DHT22 
#define LED_SDCARD_WRITE 6
#define SD_CARD_CHIP_SELECT 10
#define READ_INTERVALL_MINS 5ul

// constants 

// global objects..
RTC_DS1307 RTC;                         //  for real time clock
DHT dht(DHTPIN, DHTTYPE);               // for sensor unit
LiquidCrystal lcd(7, 8, 9, 10, 2, 3);   // the LCD display
unsigned long lastSDWrite;              // timer counter
unsigned long timerIntervall;
 
void setup(void) {
  
  // start serial output
  Serial.begin(9600);

  // Greetings on the monitor
  Serial.println("System starts...");

  // initialize LCD display
  initializeLCD();
  
  // initialize RTC
  initializeRTC();  
  
  // initialize sensor unit
  dht.begin();

  // initialize SD card
  initializeSD();
  
  // set pin für write LED setzen
  pinMode(LED_SDCARD_WRITE,OUTPUT);

  lcd.clear();
  lastSDWrite = millis();
  timerIntervall = READ_INTERVALL_MINS * 60 * 1000;

  Serial.println("Nu geit datt los...");
  Serial.println("\n---------------------------------------------\n");
}

// ---------------------------------------------------------------------------------------
void initializeLCD() {

  Serial.println("Initialize LCD");
  
  lcd.begin(16, 2);
  
  lcd.print("System");
  lcd.setCursor(0,1); 
  lcd.print("initializes");

  Serial.println("LCD initialized");
}

// ---------------------------------------------------------------------------------------
void initializeRTC() {

  Serial.println("\nInitialize real time clock ...");
  
  // initialize I2C  
  Wire.begin();
  
  RTC.begin(); 
  
  // Prüfen ob RTC läuft  
  if (! RTC.isrunning()) {
    // Aktuelles Datum und Zeit setzen, falls die Uhr noch nicht läuft
    RTC.adjust(DateTime(__DATE__, __TIME__));
    Serial.println("Clock started and set to current system date");
  }
  else Serial.println("Clock already running");
}

// ---------------------------------------------------------------------------------------
void initializeSD() {
  
  Serial.println("\nInitialize SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(SD_CARD_CHIP_SELECT)) {
    Serial.println("SD not present or could not be opened");
    // set warning light
    digitalWrite(LED_SDCARD_WRITE,HIGH);
    return;
  }
  Serial.println("SD card intialized");
}

// ---------------------------------------------------------------------------------------
void loop () {

  String currentTime;
  float currHumidity;
  float currTemperature;

  // get data from sensor
  currHumidity = dht.readHumidity();     //Luftfeuchte auslesen
  currTemperature = dht.readTemperature();  //Temperatur auslesen
 
  // check for correct number. if NaN (not a number) show error
  if (isnan(currTemperature) || isnan(currHumidity)) {
    Serial.println("DHT22 konnte nicht ausgelesen werden");
  } 
  else {

    // print on LCD display
    printCurrentData(currTemperature, currHumidity);

    // check timer
    if (millis() - lastSDWrite > timerIntervall) { 

      // save current time for timer
      lastSDWrite = millis();

      // create timestamp
      currentTime = createTimeStamp();
  
      // write to SD card timestamp and data
      writeDataToSD(currentTime, currTemperature, currHumidity);

    }
  }

  delay(1000);
}

// ---------------------------------------------------------------------------------------
void printCurrentData(float temperature, float humidity) {
  
  // lcd.clear();
  lcd.setCursor(0,0); 
  lcd.print("C: ");
  lcd.setCursor(4,0); 
  lcd.print(String(temperature,2));
  lcd.setCursor(0,1);
  lcd.print("%: ");
  lcd.setCursor(4,1);
  lcd.print(String(humidity,2));
}

// ---------------------------------------------------------------------------------------
String writeDataToSD( String timeStamp, float temperature, float humidity) {

  // start writinge LED and open file
  digitalWrite(LED_SDCARD_WRITE,HIGH);
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {

    // create log entry
    timeStamp.concat(";");
    timeStamp.concat(String(temperature,2));
    timeStamp.concat(";");
    timeStamp.concat(String(humidity,2));

    // write data and close file again
    dataFile.println(timeStamp);
    dataFile.close(); 

    // log to serial monitor as well
    Serial.println(timeStamp);

    // clear write LED
    digitalWrite(LED_SDCARD_WRITE,LOW);
  }
  else {
     // if the file isn't open, pop up an error:
    Serial.println("error opening datalog.txt");
  }

  return timeStamp;
}
 
// ---------------------------------------------------------------------------------------
String createTimeStamp() {
  
  DateTime now = RTC.now();
  String strCurrDate;
  
  strCurrDate = String(now.year());
  strCurrDate = String(strCurrDate + "-");
  strCurrDate = String(strCurrDate + make2Digits(String(now.month())));
  strCurrDate = String(strCurrDate + "-");
  strCurrDate = String(strCurrDate + make2Digits(String(now.day()))); 
  strCurrDate = String(strCurrDate + " ");
  strCurrDate = String(strCurrDate + make2Digits(String(now.hour()))); 
  strCurrDate = String(strCurrDate + ":");
  strCurrDate = String(strCurrDate + make2Digits(String(now.minute()))); 
  strCurrDate = String(strCurrDate + ":");
  strCurrDate = String(strCurrDate + make2Digits(String(now.second()))); 

  return strCurrDate;
}

// ---------------------------------------------------------------------------------------
String make2Digits(String strInput) {

  if (strInput.length() == 1) {
    return String("0") + strInput;
  }
  return strInput;
}

