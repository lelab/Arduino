#include <DS3231.h>
#include <Wire.h>    // I2C-Bibliothek einbinden


// global objects..
DS3231  RTC(SDA, SCL);

void setup(void) {
  
  // start serial output
  Serial.begin(9600);

  // initialize RTC
  Serial.println("\nInitialize real time clock ...");
  
  RTC.begin(); 

  // set initial date and time
  RTC.setTime(0, 30, 0);     // Set the time 
  RTC.setDate(23, 1, 2016);   // Set the date
 
}
 
// ---------------------------------------------------------------------------------------
void loop () {

  String currentTime;
 
  // create timestamp
  currentTime = createTimeStamp();

  Serial.println(currentTime);
  
  delay(1000);
}



// ---------------------------------------------------------------------------------------
String createTimeStamp() {
  
  String timeStamp;

  timeStamp = String(RTC.getDateStr(FORMAT_LONG, FORMAT_LITTLEENDIAN));
  timeStamp = String(timeStamp + "-");
  timeStamp = String(timeStamp + RTC.getTimeStr(FORMAT_LONG));

  return timeStamp;
}


